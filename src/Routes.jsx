import React from "react";
import App from "./App";
import AllLists from "./components/AllLists";
import { Routes, Route } from "react-router-dom";
import Header from "./components/Header";
import Error404 from "./components/Error404";
const RoutesFile = () => {
  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="/boards/:id" element={<AllLists />} />
        <Route path="*" element={<Error404 />} />
      </Routes>
    </>
  );
};

export default RoutesFile;
