import React, { useEffect, useState } from "react";
import { Box, Button, Flex, FormControl, Input } from "@chakra-ui/react";
import axios from "axios";
import { DeleteIcon } from "@chakra-ui/icons";
import AllChecklist from "./AllChecklist";
import { Modal } from "@chakra-ui/react";
import { useDisclosure } from "@chakra-ui/react";
import AddItems from "./AddItems";
import { fetchCardsData, deleteCardData, addCardData } from "../APIs";
import { useToast } from "@chakra-ui/react";

const AllCards = ({ listId }) => {
  const [allCards, setAllCards] = useState([]);
  const [adding, setAdding] = useState(false);
  const [newCard, setNewCard] = useState("");
  const [selectedCard, setSelectedCard] = useState("");
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [errors, setErrors] = useState(false);
  const [loading, setLoading] = useState(true);

  const toast = useToast();

  //==============================================================================
  useEffect(() => {
    const fetchCards = async () => {
      try {
        const res = await fetchCardsData(listId);
        setAllCards(res);
        setLoading(false);
      } catch (error) {
        console.log(error);
        setErrors(true);
      }
    };

    fetchCards();
  }, []);

  //================================================================================

  const addCard = async (cardd) => {
    try {
      if (cardd) {
        const res = await addCardData(cardd, listId);

        setAllCards([...allCards, res]);
      }
      toast({
        title: "Success",
        status: "success",
        duration: 3000,
        isClosable: true,
      });
    } catch (error) {
      console.log(error);

      toast({
        title: "Failed",
        status: "error",
        duration: 3000,
        isClosable: true,
      });
    }
  };

  //================================================================================

  const deleteCard = async (item) => {
    try {
      const res = await deleteCardData(item);

      const remaining = allCards.filter((val) => {
        return val.id !== item;
      });

      setAllCards(remaining);
      toast({
        title: "Success",
        status: "success",
        duration: 3000,
        isClosable: true,
      });
    } catch (error) {
      console.log(error);
      toast({
        title: "Failed",
        status: "error",
        duration: 3000,
        isClosable: true,
      });
    }
  };

  //===================================================================================

  const handleOpen = () => setAdding(true);

  const handleClose = () => {
    setAdding(false);
    setNewCard("");
  };

  const handleAddCard = () => {
    addCard(newCard);
    handleClose();
    setNewCard("");
  };

  const handleModal = (currId) => {
    setSelectedCard(currId);
    onOpen();
  };

  //================================================================================

  return (
    <>
      {errors ? (
        <Box>Error..</Box>
      ) : loading ? (
        <Box>Loading...</Box>
      ) : (
        <>
          {allCards &&
            allCards.map((item) => (
              <Box key={item.id}>
                <Flex
                  bg="rgb(204, 202, 200)"
                  color="black"
                  mx="0.5rem"
                  my="1rem"
                  borderRadius="5px"
                  px="10px"
                  justify="space-between"
                  align="center"
                >
                  <Button onClick={() => handleModal(item.id)}>
                    {item.name}
                  </Button>
                  <DeleteIcon
                    cursor="pointer"
                    onClick={() => deleteCard(item.id)}
                  />
                </Flex>
              </Box>
            ))}
          <Modal isOpen={isOpen} onClose={onClose} size="3xl">
            <AllChecklist id={selectedCard} onClose={onClose} />
          </Modal>

          <Box display="flex" justifyContent="center">
            {adding === false && (
              <Button onClick={handleOpen} size="md" w="15rem">
                + Add Card
              </Button>
            )}

            <AddItems
              handleAdd={handleAddCard}
              handleClose={handleClose}
              adding={adding}
              newValue={newCard}
              setNewValue={setNewCard}
            />
          </Box>
        </>
      )}
    </>
  );
};

export default AllCards;
