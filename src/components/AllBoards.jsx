import {
  Button,
  Card,
  Grid,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  Input,
  FormControl,
  Box,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import React, { useState } from "react";
import { useToast } from "@chakra-ui/react";

const AllBoards = ({ allboard, addNewBoard, errors, loading, addError }) => {
  const [adding, setAdding] = useState(false);
  const [newBoardName, setNewBoardName] = useState("");

  const navigate = useNavigate();
  const toast = useToast();

  console.log("In all boards ", addError);

  //==============================================================

  const handleOpen = () => setAdding(true);

  const handleClose = () => {
    setAdding(false);
  };

  const handleSubmit = async () => {
    try {
      await addNewBoard(newBoardName);
      toast({
        title: "Success",
        status: "success",
        duration: 3000,
        isClosable: true,
      });
    } catch (error) {
      toast({
        title: "Failed",
        status: "error",
        duration: 3000,
        isClosable: true,
      });
    }
    setNewBoardName("");
    handleClose();
  };

  //================================================================

  return (
    <>
      {errors ? (
        <Box display="flex" justifyContent="center" fontSize="3rem">
          Error
        </Box>
      ) : loading ? (
        <Box display="flex" justifyContent="center" fontSize="3rem">
          Loading...
        </Box>
      ) : (
        <Grid templateColumns="repeat(6, 1fr)" h="100vh">
          {allboard &&
            allboard.map((item) => (
              <Card
                key={item.id}
                mx="2rem"
                my="1rem"
                h="5rem"
                w="15rem"
                p="10px"
                bg="rgb(128, 130, 245)"
                color="white"
                fontWeight="800"
                onClick={() => navigate(`boards/${item.id}`)}
              >
                {item.name}
              </Card>
            ))}
          <Button onClick={handleOpen} mx="2.3rem" my="1rem">
            Add Board
          </Button>
          <Modal isOpen={adding} onClose={handleClose}>
            <ModalContent>
              <ModalHeader>Add New Board</ModalHeader>
              <ModalCloseButton />
              <form
                onSubmit={(e) => {
                  e.preventDefault();
                  handleSubmit();
                }}
              >
                <ModalBody>
                  <FormControl>
                    <Input
                      type="text"
                      value={newBoardName}
                      onChange={(e) => setNewBoardName(e.target.value)}
                      placeholder="Enter Board Name"
                    />
                  </FormControl>
                </ModalBody>
                <ModalFooter gap="10px">
                  <Button color="blue" type="submit">
                    Add
                  </Button>
                  <Button onClick={handleClose}>Cancel</Button>
                </ModalFooter>
              </form>
            </ModalContent>
          </Modal>
        </Grid>
      )}
    </>
  );
};

export default AllBoards;
