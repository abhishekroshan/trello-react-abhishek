import React, { useEffect, useState } from "react";

import { Box, Button, Flex } from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";
import { Checkbox, CheckboxGroup } from "@chakra-ui/react";
import { Progress } from "@chakra-ui/react";
import {
  fetchCheckItemsData,
  addCheckItemData,
  deleteCheckItemData,
  updateChecked,
} from "../APIs";

import AddItems from "./AddItems";
import { useToast } from "@chakra-ui/react";

const AllCheckitems = ({ id, cardId }) => {
  const [checkItems, setCheckItems] = useState([]);
  const [newCheckItem, setNewCheckItem] = useState("");
  const [adding, setAdding] = useState(false);
  const [errors, setErrors] = useState(false);
  const [loading, setLoading] = useState(true);

  const toast = useToast();

  useEffect(() => {
    const fetchCheckItems = async () => {
      try {
        const res = await fetchCheckItemsData(id);
        setCheckItems(res);
        setLoading(false);
      } catch (error) {
        console.log(error);
        setErrors(true);
      }
    };

    fetchCheckItems();
  }, []);

  //================================================================================================

  const addCheckItem = async (itemName) => {
    try {
      const res = await addCheckItemData(id, itemName);

      setCheckItems([...checkItems, res]);
      toast({
        title: "Success",
        status: "success",
        duration: 3000,
        isClosable: true,
      });
    } catch (error) {
      console.log(error);
      toast({
        title: "Failed",
        status: "error",
        duration: 3000,
        isClosable: true,
      });
    }
  };

  //======================================================================================================

  const deleteCheckItem = async (idd) => {
    try {
      const res = await deleteCheckItemData(id, idd);

      const left = checkItems.filter((item) => {
        return item.id !== idd;
      });

      setCheckItems(left);
      toast({
        title: "Success",
        status: "success",
        duration: 3000,
        isClosable: true,
      });
    } catch (error) {
      console.log(error);
      toast({
        title: "Failed",
        status: "error",
        duration: 3000,
        isClosable: true,
      });
    }
  };

  //===============================================================================================

  const updateCheckedValue = async (itemId, newState) => {
    try {
      const res = await updateChecked(id, cardId, itemId, newState);

      const updateItems = checkItems.map((item) => {
        if (item.id === itemId) {
          return { ...item, state: newState };
        }
        return item;
      });
      setCheckItems(updateItems);
    } catch (error) {
      console.log(error);
    }
  };

  //===================================================================================================

  const calculateProgress = () => {
    let completeCount = 0;
    checkItems.forEach((item) => {
      if (item.state === "complete") {
        completeCount++;
      }
    });

    const value = (completeCount / checkItems.length) * 100;

    return value;
  };

  //==================================================================================================

  const handleOpen = () => setAdding(true);

  const handleClose = () => {
    setAdding(false);
    setNewCheckItem("");
  };

  const handleAddCheckItem = () => {
    addCheckItem(newCheckItem);
    handleClose();
  };

  //===============================================================================================

  return (
    <>
      {errors ? (
        <Flex justify="center" fontSize="1rem">
          Error..
        </Flex>
      ) : loading ? (
        <Flex justify="center" fontSize="1rem">
          Loading...
        </Flex>
      ) : (
        <>
          <Box
            bg="rgb(184, 185, 209)"
            mx="20px"
            my="10px"
            borderRadius="10px"
            p="10px"
          >
            {checkItems &&
              checkItems.map((item) => (
                <Flex key={item.id} py="5px" justify="space-between">
                  <Flex gap="10px">
                    <Checkbox
                      isChecked={item.state === "complete" ? true : false}
                      onChange={(e) =>
                        updateCheckedValue(
                          item.id,
                          e.target.checked ? "complete" : "incomplete"
                        )
                      }
                    />

                    <Box key={item.id}>{item.name}</Box>
                  </Flex>
                  <CloseIcon
                    onClick={() => deleteCheckItem(item.id)}
                    cursor="pointer"
                  />
                </Flex>
              ))}

            <Progress value={calculateProgress()} m="10px" />

            {adding === false && (
              <Button onClick={handleOpen} size="sm">
                + CheckItem
              </Button>
            )}

            <AddItems
              handleAdd={handleAddCheckItem}
              handleClose={handleClose}
              adding={adding}
              newValue={newCheckItem}
              setNewValue={setNewCheckItem}
            />
          </Box>
        </>
      )}
    </>
  );
};

export default AllCheckitems;
