import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import AllCards from "./AllCards";
import Error404 from "./Error404";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  Grid,
  Flex,
  Box,
  CardFooter,
} from "@chakra-ui/react";

import { fetchListsData, addNewList, deleteTheList } from "../APIs";

import { DeleteIcon } from "@chakra-ui/icons";
import AddItems from "./AddItems";
import { useToast } from "@chakra-ui/react";

const AllLists = () => {
  const [allLists, setAllLists] = useState([]);
  const [newListName, setNewListName] = useState("");
  const [adding, setAdding] = useState(false);
  const [errors, setErrors] = useState(false);
  const [loading, setLoading] = useState(true);

  const { id } = useParams();

  const toast = useToast();

  //========================================================================================

  useEffect(() => {
    const fetchLists = async () => {
      try {
        const res = await fetchListsData(id);
        setAllLists(res);
        console.log(res);
        setLoading(false);
      } catch (error) {
        console.log("kkk", error);
        setErrors(true);
      }
    };

    fetchLists();
  }, []);

  console.log("err", errors);

  //==============================================================================================

  const addList = async (namee) => {
    try {
      const res = await addNewList(namee, id);
      setAllLists([...allLists, res]);

      toast({
        title: "Success",
        status: "success",
        duration: 3000,
        isClosable: true,
      });
    } catch (error) {
      console.log(error);
      toast({
        title: "Failed",
        status: "error",
        duration: 3000,
        isClosable: true,
      });
    }
  };

  //==============================================================================================

  const deleteList = async (value) => {
    try {
      const res = await deleteTheList(value);
      const newValue = allLists.filter((item) => item.id !== value);

      setAllLists(newValue);

      toast({
        title: "Success",
        status: "success",
        duration: 3000,
        isClosable: true,
      });
    } catch (error) {
      toast({
        title: "Failed",
        status: "error",
        duration: 3000,
        isClosable: true,
      });
      console.log(error);
    }
  };

  //==============================================================================================

  const handleOpen = () => setAdding(true);

  const handleClose = () => {
    setAdding(false);
    setNewListName("");
  };

  const handleAddList = () => {
    if (newListName !== "") {
      addList(newListName);
      handleClose();
    } else {
      handleClose();
    }
  };
  return (
    <>
      {errors ? (
        <Box>
          <Error404 />
        </Box>
      ) : loading ? (
        <Box display="Flex" justifyContent="center" fontSize="3rem">
          Loading...
        </Box>
      ) : (
        <Grid mx="1rem" my="2rem" templateColumns="repeat(6, 1fr)">
          {allLists &&
            allLists.map((item) => (
              <Card
                key={item.id}
                mx="0.5rem"
                color="white"
                bg="rgb(0,0,0)"
                fontWeight="800"
                h="fit-content"
              >
                <CardHeader key={item.id}>
                  <Flex justify="space-between">
                    {item.name}
                    <DeleteIcon
                      onClick={() => deleteList(item.id)}
                      cursor="pointer"
                    />
                  </Flex>
                </CardHeader>
                <CardBody>
                  <AllCards listId={item.id} />
                </CardBody>
                <CardFooter></CardFooter>
              </Card>
            ))}
          {adding === false && (
            <Button onClick={handleOpen} mx="10px">
              + Add Another List
            </Button>
          )}
          <AddItems
            handleAdd={handleAddList}
            handleClose={handleClose}
            adding={adding}
            newValue={newListName}
            setNewValue={setNewListName}
          />
        </Grid>
      )}
    </>
  );
};

export default AllLists;
