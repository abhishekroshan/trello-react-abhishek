import React, { useEffect, useState } from "react";

import AllCheckitems from "./AllCheckitems";
import {
  fetchCheckListData,
  addCheckListData,
  deleteCheckListData,
} from "../APIs";
import AddItems from "./AddItems";
import { useToast } from "@chakra-ui/react";

import { Box, Button, Flex } from "@chakra-ui/react";
import {
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
} from "@chakra-ui/react";
import { DeleteIcon } from "@chakra-ui/icons";

const AllChecklist = ({ id }) => {
  const [checklist, setChecklist] = useState([]);
  const [adding, setAdding] = useState(false);
  const [newChecklist, setNewChecklist] = useState("");
  const [errors, setErrors] = useState(false);
  const [loading, setLoading] = useState(true);

  const toast = useToast();

  //===============================================================================

  useEffect(() => {
    const fetchChecklist = async () => {
      try {
        const res = await fetchCheckListData(id);
        setChecklist(res);
        setLoading(false);
      } catch (error) {
        console.log(error);
        setErrors(true);
      }
    };

    fetchChecklist();
  }, []);

  //===============================================================================

  const addCheckList = async (val) => {
    try {
      const res = await addCheckListData(id, val);

      setChecklist([...checklist, res]);
      toast({
        title: "Success",
        status: "success",
        duration: 3000,
        isClosable: true,
      });
    } catch (error) {
      console.log(error);

      toast({
        title: "Failed",
        status: "error",
        duration: 3000,
        isClosable: true,
      });
    }
  };

  //================================================================================

  const deleteChecklist = async (iidd) => {
    try {
      const res = await deleteCheckListData(id, iidd);

      const rem = checklist.filter((item) => item.id !== iidd);

      setChecklist(rem);
      toast({
        title: "Success",
        status: "success",
        duration: 3000,
        isClosable: true,
      });
    } catch (error) {
      console.log(error);
      toast({
        title: "Failed",
        status: "error",
        duration: 3000,
        isClosable: true,
      });
    }
  };

  //==================================================================================

  const handleOpen = () => setAdding(true);

  const handleClose = () => {
    setAdding(false);
    setNewChecklist("");
  };

  const handleAddChecklist = () => {
    addCheckList(newChecklist);
    handleClose();
  };

  //==================================================================================

  return (
    <>
      {errors ? (
        <ModalBody>
          <ModalContent>
            <ModalBody>Error..</ModalBody>
          </ModalContent>
        </ModalBody>
      ) : loading ? (
        <ModalBody>
          <ModalContent>
            <ModalBody fontSize="1.5rem">Loading...</ModalBody>
          </ModalContent>
        </ModalBody>
      ) : (
        <>
          <ModalBody>
            <ModalOverlay />
            <ModalContent bg="rgb(218, 220, 224)">
              <ModalHeader>CheckList</ModalHeader>
              <ModalCloseButton />
              <ModalBody>
                <Flex justify="space-between">
                  <Flex
                    direction="column"
                    border="1px solid black"
                    flexGrow="1"
                    px="20px"
                    py="10px"
                    fontWeight="600"
                    bg="black"
                    color="white"
                  >
                    {checklist.map((item) => (
                      <Box key={item.id}>
                        <Flex justifyContent="space-between">
                          <Box key={item.id}>{item.name}</Box>
                          <DeleteIcon
                            cursor="pointer"
                            onClick={() => deleteChecklist(item.id)}
                          />
                        </Flex>
                        <AllCheckitems id={item.id} cardId={id} />
                      </Box>
                    ))}
                  </Flex>
                  <Flex width="12rem" justify="center">
                    {adding === false && (
                      <Button
                        p="10px"
                        onClick={handleOpen}
                        bg="rgb(94, 143, 224)"
                      >
                        Add CheckList
                      </Button>
                    )}

                    <AddItems
                      handleAdd={handleAddChecklist}
                      handleClose={handleClose}
                      adding={adding}
                      newValue={newChecklist}
                      setNewValue={setNewChecklist}
                    />
                  </Flex>
                </Flex>
              </ModalBody>
            </ModalContent>
          </ModalBody>
        </>
      )}
    </>
  );
};

export default AllChecklist;
