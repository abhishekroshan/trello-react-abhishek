import React, { useState } from "react";
import { Button, Flex, FormControl, Input } from "@chakra-ui/react";

const AddItems = ({
  handleAdd,
  handleClose,
  adding,
  newValue,
  setNewValue,
}) => {
  return (
    <>
      {adding && (
        <Flex direction="column">
          <form
            onSubmit={(e) => {
              e.preventDefault();
              handleAdd();
            }}
          >
            <FormControl>
              <Input
                type="text"
                value={newValue}
                onChange={(e) => {
                  setNewValue(e.target.value);
                }}
                placeholder="Enter Name"
              />
            </FormControl>
            <Flex justify="space-between" my="10px">
              <Button onClick={handleAdd}>Add</Button>
              <Button onClick={handleClose}>X</Button>
            </Flex>
          </form>
        </Flex>
      )}
    </>
  );
};

export default AddItems;
