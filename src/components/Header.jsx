import { Box, Flex, Image } from "@chakra-ui/react";
import React from "react";
import trelloImage from "../assets/greyTrello.png";
import { useNavigate } from "react-router-dom";

const Header = () => {
  const navigate = useNavigate();
  return (
    <Flex h="4rem" px="3rem" bg="rgb(52, 52, 61)">
      <Image src={trelloImage} onClick={() => navigate("/")} cursor="pointer" />
    </Flex>
  );
};

export default Header;
