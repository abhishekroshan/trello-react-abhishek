import { useEffect, useState } from "react";
import "./App.css";
import AllBoards from "./components/AllBoards";

import { fetchBoardsData, addBoard } from "./APIs";

function App() {
  const [boards, setBoards] = useState([]);
  const [errors, setErrors] = useState(false);
  const [loading, setLoading] = useState(true);

  //===================================================================

  const addNewBoard = async (newBoardData) => {
    try {
      const res = await addBoard(newBoardData);
      setBoards((prevBoards) => [...prevBoards, res]);
    } catch (error) {
      throw error;
    }
  };

  //===================================================================

  useEffect(() => {
    const fetchBoard = async () => {
      try {
        const data = await fetchBoardsData();

        setBoards(data);
        setLoading(false);
      } catch (error) {
        console.log(error);
        setErrors(true);
      }
    };

    fetchBoard();
  }, []);

  console.log(boards);

  //==================================================================

  return (
    <>
      <AllBoards
        allboard={boards}
        addNewBoard={addNewBoard}
        errors={errors}
        loading={loading}
      />
    </>
  );
}

export default App;
