import axios from "axios";

const key = import.meta.env.VITE_API_KEY;
const token = import.meta.env.VITE_API_TOKEN;

//===========================================================================================
axios.defaults.params = {
  key: key,
  token: token,
};

export const fetchBoardsData = async () => {
  try {
    const res = await axios.get(`https://api.trello.com/1/members/me/boards`);
    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const addBoard = async (newBoardData) => {
  try {
    const res = await axios.post(
      `https://api.trello.com/1/boards/?name=${newBoardData}`
    );

    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

//=======================================================================================================

export const fetchListsData = async (id) => {
  try {
    const res = await axios.get(`https://api.trello.com/1/boards/${id}/lists`);
    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const addNewList = async (namee, id) => {
  try {
    const res = await axios.post(
      `https://api.trello.com/1/lists?name=${namee}&idBoard=${id}`
    );

    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const deleteTheList = async (value) => {
  try {
    const res = await axios.put(
      `https://api.trello.com/1/lists/${value}/closed?value=true`
    );

    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

//======================================================================================================

export const fetchCardsData = async (id) => {
  try {
    const res = await axios.get(`https://api.trello.com/1/lists/${id}/cards`);

    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const deleteCardData = async (item) => {
  try {
    const res = await axios.delete(`https://api.trello.com/1/cards/${item}`);

    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const addCardData = async (cardd, listId) => {
  try {
    if (cardd) {
      const res = await axios.post(
        `https://api.trello.com/1/cards?name=${cardd}&idList=${listId}`
      );

      return res.data;
    }
  } catch (error) {
    console.log(error);
    throw error;
  }
};

//=============================================================================================================

export const fetchCheckListData = async (id) => {
  try {
    const res = await axios.get(
      `https://api.trello.com/1/cards/${id}/checklists`
    );

    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const addCheckListData = async (id, val) => {
  try {
    const res = await axios.post(
      `https://api.trello.com/1/cards/${id}/checklists?name=${val}`
    );

    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const deleteCheckListData = async (id, cListId) => {
  try {
    const res = await axios.delete(
      `https://api.trello.com/1/cards/${id}/checklists/${cListId}`
    );

    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

//===========================================================================================================

export const fetchCheckItemsData = async (id) => {
  try {
    const res = await axios.get(
      `https://api.trello.com/1/checklists/${id}/checkItems`
    );

    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const addCheckItemData = async (id, itemName) => {
  try {
    const res = await axios.post(
      `https://api.trello.com/1/checklists/${id}/checkItems?name=${itemName}`
    );

    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const deleteCheckItemData = async (id, checkItemId) => {
  try {
    const res = await axios.delete(
      `https://api.trello.com/1/checklists/${id}/checkItems/${checkItemId}`
    );

    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const updateChecked = async (id, cardId, itemId, newState) => {
  try {
    const res = await axios.put(
      `https://api.trello.com/1/cards/${cardId}/checklist/${id}/checkItem/${itemId}?&state=${newState}`
    );

    return res.data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};
